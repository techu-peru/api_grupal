package net.techu.api.controllers;

import com.mongodb.lang.Nullable;
import net.techu.api.models.Pelicula;
import net.techu.api.services.PeliculaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("peliculas")
public class PeliculaController {

    private final PeliculaService peliculaService;
    private Pelicula pelicula;

    @Autowired
    public PeliculaController(PeliculaService peliculaService)
    {
        this.peliculaService = peliculaService;

    }

    @GetMapping("/peliculas")
    public ResponseEntity<List<Pelicula>> peliculas() {

        return ResponseEntity.ok(peliculaService.findAll());
    }

    @PostMapping("/peliculas")
    public ResponseEntity<Object> savePelicula(@RequestBody Pelicula pelicula)
    {
        Pelicula p = peliculaService.findOne(pelicula.getCodigo());
        if (p == null){
            return new ResponseEntity<Object>(peliculaService.savePelicula(pelicula),HttpStatus.OK);
        }
            else
                {
                return new ResponseEntity<Object>("Codigo de pelicula ya existe",HttpStatus.CONFLICT);

            }
    }

    @RequestMapping(value = "/peliculas/{codigo}", method = RequestMethod.GET)
    public ResponseEntity<Object> getPelicula(@PathVariable("codigo") String codigo) {
        Pelicula p = peliculaService.findOne(codigo);
        if (p != null){
            return new ResponseEntity<Object>(p, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<Object>("Pelicula no encontrada", HttpStatus.NOT_FOUND);
        }

    }
    @PutMapping(value="/peliculas/{codigo}")
    public ResponseEntity<Object> updatePeliculas(@PathVariable("codigo") String codigo, @RequestBody Pelicula pelicula) {
            Pelicula p = peliculaService.findOne(codigo);
            if (p != null){
                pelicula.setCodigo(codigo);
                peliculaService.updatePelicula(pelicula);
                return new ResponseEntity<Object>("Pelicula " + codigo + " actualizado", HttpStatus.OK);}
            else{
                return new ResponseEntity<Object>("Pelicula " + codigo + " no existe", HttpStatus.NOT_FOUND);}
    }

    @DeleteMapping(value="/peliculas/{codigo}")
    public ResponseEntity<Object> deletePeliculas(@PathVariable("codigo") String codigo) {
        Pelicula p = peliculaService.findOne(codigo);
        if (p != null) {
            peliculaService.deletePelicula(codigo);
            return new ResponseEntity<Object>("Pelicula " + codigo + " eliminada", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<Object>("Pelicula " + codigo + " no existe", HttpStatus.NOT_FOUND);
        }


    }





}



