package net.techu.api.repository;

import net.techu.api.models.Pelicula;

import java.util.List;

public interface PeliculaRepository {
    List<Pelicula> findAll();
    public Pelicula savePelicula(Pelicula p);
    public void deletePelicula (String codigo);
    public void updatePelicula(Pelicula p);
    public Pelicula findOne(String codigo);
}
