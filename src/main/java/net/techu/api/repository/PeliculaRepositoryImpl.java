package net.techu.api.repository;

import net.techu.api.models.Pelicula;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PeliculaRepositoryImpl implements PeliculaRepository{

    private final MongoOperations mongoOperations;

    @Autowired
    public PeliculaRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Pelicula> findAll() {
        List<Pelicula> peliculas = this.mongoOperations.find(new Query(), Pelicula.class);
        return peliculas;
    }

    @Override
    public Pelicula savePelicula(Pelicula p) {
        this.mongoOperations.save(p);
        return findOne(p.getCodigo());
    }

    @Override
    public void deletePelicula(String codigo) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("codigo").is(codigo)), Pelicula.class);
    }

    @Override
    public void updatePelicula(Pelicula p) {
        this.mongoOperations.save(p);
    }

    @Override
    public Pelicula findOne(String codigo) {
        Pelicula encontrado = this.mongoOperations.findOne(new Query(Criteria.where("codigo").is(codigo)),Pelicula.class);
        return encontrado;
    }
}
