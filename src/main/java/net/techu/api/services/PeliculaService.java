package net.techu.api.services;

import net.techu.api.models.Pelicula;

import java.util.List;

public interface PeliculaService {
    List<Pelicula> findAll();
    public Pelicula savePelicula(Pelicula p);
    public void deletePelicula (String codigo);
    public void updatePelicula(Pelicula p);
    public Pelicula findOne(String codigo);
}
