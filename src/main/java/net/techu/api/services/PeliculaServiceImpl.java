package net.techu.api.services;

import net.techu.api.models.Pelicula;
import net.techu.api.repository.PeliculaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service ("peliculaService")
@Transactional
public class  PeliculaServiceImpl implements PeliculaService{
    private PeliculaRepository peliculaRepository;
    @Autowired
    public PeliculaServiceImpl(PeliculaRepository peliculaRepository)
    {
        this.peliculaRepository = peliculaRepository;
    }

    @Override
    public List<Pelicula> findAll() {
        return peliculaRepository.findAll();
    }

    @Override
    public Pelicula findOne(String codigo) {
        return peliculaRepository.findOne(codigo);
    }

    @Override
    public Pelicula savePelicula(Pelicula p) {
        return peliculaRepository.savePelicula(p);
    }

    @Override
    public void updatePelicula(Pelicula p) {
        peliculaRepository.updatePelicula(p);
    }

    @Override
    public void deletePelicula(String codigo) {
        peliculaRepository.deletePelicula(codigo);
    }
}
